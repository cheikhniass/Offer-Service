
<%@include file="includes/header.jsp"%>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Edit offer</h3>
	</div>
	<div class="panel-body">
		<form:form modelAttribute="editOfferForm" role="form">
			<form:errors />
			<div class="form-group">
				<form:label path="text"> Edit offer</form:label>
				<form:input path="text" class="form-control" id="text"
					placeholder="Enter your offer" />
				<form:errors path="text" cssClass="error" />
			</div>



			<button type="submit" class="btn btn-default">edit Offer</button>
		</form:form>
	</div>
</div>
<%@include file="includes/footer.jsp"%>