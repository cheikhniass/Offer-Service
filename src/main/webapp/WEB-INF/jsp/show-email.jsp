<%@include file="includes/header.jsp"%>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Please Sign up</h3>
	</div>
	<div class="panel-body">
		<form:form modelAttribute="emailForm" role="form">
			<form:errors />
			<div class="form-group">
				<form:label path="from">From</form:label>
				<form:input path="from" type="email" class="form-control"
					placeholder="Enter email" />
				<form:errors path="from" cssClass="error" />
				<p class="help-block"></p>
			</div>
			<div class="form-group">
				<form:label path="to">To</form:label>
				<form:input path="to" type="email" class="form-control"
					id="exampleInputEmail1" placeholder="Enter email" />
				<form:errors path="to" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="subject"> Subject</form:label>
				<form:input path="subject" class="form-control"
					id="exampleInputEmail1" placeholder="Enter email" />
				<form:errors path="subject" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="body">Body</form:label>
				<form:input path="body" class="form-control" id="exampleInputEmail1"
					placeholder="Enter email" />
				<form:errors path="body" cssClass="error" />
			</div>

			<button type="submit" class="btn btn-default">Send an Email</button>

		</form:form>


	</div>
</div>
<%@include file="includes/footer.jsp"%>
