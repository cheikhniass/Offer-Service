<%@include file="includes/header.jsp"%>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">List of current offers</h3>
	</div>
	<div class="panel-body">
		<c:forEach var="offer" items="${offers}">


			<dl class="dl-horizontal">

				<dt>Name</dt>
				<dd>
					<c:out value="${offer.user.name}" />
				</dd>
				<dt>email</dt>
				<dd>
					<c:out value="${offer.user.email}" />
				</dd>
				<dt>Text</dt>
				<dd>
					<c:out value="${offer.text}" />
				</dd>
			</dl>
			<a class="btn btn-link"
				href="<c:url value ='/offers/${offer.user.id}/send-email'/>">Send
				an email</a>

		</c:forEach>

	</div>
</div>
<%@include file="includes/footer.jsp"%>