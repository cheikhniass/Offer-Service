
<%@include file="includes/header.jsp"%>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Change Password</h3>
	</div>
	<div class="panel-body">
		<form:form modelAttribute="changePasswordForm" role="form">
			<form:errors />
			<div class="form-group">
				<form:label path="newPassword">Password</form:label>
				<form:password path="newPassword" class="form-control"
					id="exampleInputPassword1" placeholder="Password" />
				<form:errors path="newPassword" cssClass="error" />
			</div>

			<div class="form-group">
				<form:label path="confirmPassword">Password</form:label>
				<form:password path="confirmPassword" class="form-control"
					id="exampleInputPassword1" placeholder="Password" />
				<form:errors path="confirmPassword" cssClass="error" />
			</div>

			<button type="submit" class="btn btn-default">Change
				password</button>
		</form:form>
	</div>
</div>
<%@include file="includes/footer.jsp"%>