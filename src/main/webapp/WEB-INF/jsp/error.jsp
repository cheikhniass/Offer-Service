<%@include file="includes/header.jsp"%>
<div class="alert alert-danger" role="alert">
<p>There was an expected error (type = ${error}, status ${status} ) : <i> ${message} </i></p>
<p>Click<a href = "http://localhost:8080"> here to go back to home page</a></p>
<%@include file="includes/footer.jsp"%>
</div>