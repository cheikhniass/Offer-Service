package com.niass.offer.repo;

import java.util.List;

import javax.swing.CellEditor;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.niass.offer.abstractSession.OffferSession;
import com.niass.offer.entity.Offer;
import com.niass.offer.entity.OfferUser;

@Repository
@Transactional()
public class OfferRepoImpl extends OffferSession implements OfferRepo {

	@Override
	public void saveOrUpdate(Offer newOffer) {
		session().save(newOffer);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Offer> getOfferByLoggedInUser(long id) {
		Criteria cr = session().createCriteria(Offer.class);
		cr.createAlias("user", "u");
		cr.add(Restrictions.eq("u.id", id));
		cr.add(Restrictions.eq("u.isEnable", true));

		return cr.list();
	}

	
	@Override
	public Offer findByOneOffer(long id) {
		Criteria cr = session().createCriteria(Offer.class);
		cr.add(Restrictions.eq("id", id));

		return (Offer) cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Offer>getCurrentOffers() {
		Criteria cr = session().createCriteria(Offer.class);
		 return cr.list();
		
	}

	@Override
	public void remove(Offer offer) {
		session().delete(offer);
		
	}

}
