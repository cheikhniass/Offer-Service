package com.niass.offer.repo;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.niass.offer.abstractSession.OffferSession;
import com.niass.offer.entity.OfferUser;

@Repository
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserOfferRepoImpl extends OffferSession implements UserOfferRepo {

	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(OfferUser user) {
		session().saveOrUpdate(user);

	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public OfferUser findyEmail(String emailAddress) {
		Criteria cr = session().createCriteria(OfferUser.class);
		OfferUser offerUser = (OfferUser) cr.add(
				Restrictions.eq("email", emailAddress)).uniqueResult();
		return offerUser;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public OfferUser findByVerificationCode(String verificationCode) {
		Criteria cr = session().createCriteria(OfferUser.class);
		OfferUser offerUser = (OfferUser) cr.add(
				Restrictions.eq("verificationCode", verificationCode))
				.uniqueResult();
		return offerUser;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public OfferUser findByOne(long loggedInuser) {
		Criteria cr = session().createCriteria(OfferUser.class);
		OfferUser offerUser = (OfferUser) cr.add(
				Restrictions.eq("id", loggedInuser))
				.uniqueResult();
		return offerUser;
		
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public OfferUser findByforgotPasswordVerificationCode(String token) {
		Criteria cr = session().createCriteria(OfferUser.class);
		OfferUser offerUser = (OfferUser) cr.add(
				Restrictions.eq("forgotPasswordVerificationCode", token))
				.uniqueResult();
		return offerUser;
	}

	@Override
	public void delete(OfferUser user) {
		session().delete(user);
		
	}

}
