package com.niass.offer.repo;

import java.util.List;

import com.niass.offer.entity.Offer;
import com.niass.offer.entity.OfferUser;

public interface OfferRepo {

	void saveOrUpdate(Offer newOffer);

	List<Offer> getOfferByLoggedInUser(long id);

	Offer findByOneOffer(long id);

	List<Offer> getCurrentOffers();

	void remove(Offer offer);

}