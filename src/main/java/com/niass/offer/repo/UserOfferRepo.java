package com.niass.offer.repo;

import com.niass.offer.entity.OfferUser;


public interface UserOfferRepo {

	public abstract void saveOrUpdate(OfferUser user);

	public abstract OfferUser findyEmail(String emailAddress);

	public abstract OfferUser findByVerificationCode(String verificationCode);

	public abstract OfferUser findByOne(long loggedInuser);

	public abstract OfferUser findByforgotPasswordVerificationCode(String token);

	public abstract void delete(OfferUser user);

}