package com.niass.offer.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.niass.offer.dto.ForgotPasswordForm;
import com.niass.offer.dto.ResetPasswordForm;
import com.niass.offer.dto.SignupForm;
import com.niass.offer.service.UserServiceOffer;
import com.niass.offer.util.MyUtil;
import com.niass.offer.validator.ForgotPasswordValidator;
import com.niass.offer.validator.OfferUserSignupFormValidator;
import com.niass.offer.validator.ResetPasswordValidator;

@Controller
public class HomeController {
	private UserServiceOffer UserServiceOffer;
	private OfferUserSignupFormValidator signupValidator;
	private ForgotPasswordValidator forgotPasswordValidator;
	private ResetPasswordValidator resetPasswordValidator;

	@Autowired
	public HomeController(
			com.niass.offer.service.UserServiceOffer userServiceOffer,
			OfferUserSignupFormValidator signupValidator,
			ForgotPasswordValidator forgotPasswordValidator,
			ResetPasswordValidator resetPasswordValidator) {
		super();
		this.forgotPasswordValidator = forgotPasswordValidator;
		UserServiceOffer = userServiceOffer;
		this.signupValidator = signupValidator;
		this.resetPasswordValidator = resetPasswordValidator;
		
	}

	@InitBinder("signupForm")
	protected void initSignUpBinder(WebDataBinder binder) {
		binder.setValidator(signupValidator);
	}

	@InitBinder("forgotPasswordForm")
	protected void initForgotPasswordBinder(WebDataBinder binder) {
		binder.setValidator(forgotPasswordValidator);
	}
	@InitBinder("resetPasswordForm")
	protected void iniresetPasswordBinder(WebDataBinder binder) {
		binder.setValidator(resetPasswordValidator);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {
		model.addAttribute(new SignupForm());
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signup(
			@ModelAttribute("signupForm") @Valid SignupForm signupForm,
			BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "signup";

		}
		System.out.println("********contrpller" + signupForm);
		UserServiceOffer.signUp(signupForm);
		MyUtil.flah(redirectAttributes, "success", "signupSucess");
		return "redirect:/";

	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.GET)
	public String forgotPassword(Model model) {
		model.addAttribute(new ForgotPasswordForm());
		return "forgot-password";
	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.POST)
	public String forgotPassword(
			@ModelAttribute("forgotPasswordForm") @Valid ForgotPasswordForm form,
			BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "forgot-password";
		}
		UserServiceOffer.sendForgotPasswordtoken(form);
		MyUtil.flah(redirectAttributes, "success", "forgotPasswordSuccess");
		return "redirect:/";

	}

	@RequestMapping(value = "/reset-password/{token}", method = RequestMethod.GET)
	public String resetPassword(Model model) {
		model.addAttribute(new ResetPasswordForm());
		return "reset-password";
	}

	@RequestMapping(value = "/reset-password/{token}", method = RequestMethod.POST)
	public String resetPassword(@PathVariable("token") String token,
			@ModelAttribute("resetPasswordForm") @Valid ResetPasswordForm form,
			BindingResult result, RedirectAttributes redirectAttributes,
			HttpServletRequest request) throws ServletException {
		if (result.hasErrors()) {
			return "reset-password";

		}
		UserServiceOffer.resetPassword(form, token, result);
		MyUtil.flah(redirectAttributes, "success", "resetPasswordSuccess");
		request.logout();
		return "redirect:/";

	}

}
