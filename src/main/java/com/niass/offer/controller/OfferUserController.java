package com.niass.offer.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.niass.offer.dto.ChangeEmailForm;
import com.niass.offer.dto.ChangePasswordForm;
import com.niass.offer.entity.OfferUser;
import com.niass.offer.service.UserServiceOffer;
import com.niass.offer.util.MyUtil;
import com.niass.offer.validator.ChangeEmailValidator;
import com.niass.offer.validator.ChangePasswordValidator;
import com.niass.offer.validator.UserEdit;

@Controller
@RequestMapping("/users")
public class OfferUserController {
	private UserServiceOffer userServiceOffer;
	private ChangePasswordValidator changePasswordValidator;
	private ChangeEmailValidator changeEmailValidator;

	@Autowired
	public OfferUserController(UserServiceOffer userServiceOffer,
			ChangePasswordValidator changePasswordValidator, ChangeEmailValidator changeEmailValidator) {
		this.userServiceOffer = userServiceOffer;
		this.changePasswordValidator = changePasswordValidator;
		this.changeEmailValidator = changeEmailValidator;

	}

	@InitBinder("changePasswordForm")
	protected void initChangePasswordBinder(WebDataBinder binder) {
		binder.setValidator(changePasswordValidator);
	}
	@InitBinder("changeEmailForm")
	protected void initChangeEmailValidatorBinder(WebDataBinder binder) {
		binder.setValidator(changeEmailValidator);
	}


	@RequestMapping("/{verificationCode}/verify")
	public String verify(
			@PathVariable("verificationCode") String verificationCode,
			RedirectAttributes redirectAttributes, HttpServletRequest request)
			throws ServletException {

		userServiceOffer.verify(verificationCode);
		MyUtil.flah(redirectAttributes, "success", "verificationSucess");
		request.logout();
		return "redirect:/";

	}

	@RequestMapping("/resend-verication-mail")
	public String resendverificationcode(RedirectAttributes redirectAttributes,
			HttpServletRequest request) throws ServletException {
		userServiceOffer.resendVerifiacationCode();
		MyUtil.flah(redirectAttributes, "success", "resendverificationsucess");
		request.logout();

		return "redirect:/";
	}

	@RequestMapping("/{userId}")
	public String findOne(@PathVariable("userId") long userId, Model model) {
		OfferUser user = userServiceOffer.findOne(userId);
		model.addAttribute("user", user);
		return "user";

	}

	@RequestMapping("/{userId}/edit")
	public String userEdit(@PathVariable("userId") long userId, Model model) {
		OfferUser user = userServiceOffer.findOne(userId);
		UserEdit userEdit = new UserEdit();
		userEdit.setName(user.getName());
		userEdit.setRoles(user.getRoles());
		model.addAttribute("userEdit", userEdit);
		return "user-edit";

	}

	@RequestMapping(value = "/{userId}/edit", method = RequestMethod.POST)
	public String userEdit(@PathVariable("userId") Long userId,
			@ModelAttribute("userEdit") @Valid UserEdit edit,
			BindingResult result, RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "user-edit";
		}
		userServiceOffer.edit(userId, edit);
		MyUtil.flah(redirectAttributes, "success", "editsuccess");
		return "redirect:/";

	}

	@RequestMapping("/{userId}/change-password")
	public String changePassword(Model model) {
		model.addAttribute(new ChangePasswordForm());
		return "change-password";

	}

	@RequestMapping(value = "/{userId}/change-password", method = RequestMethod.POST)
	public String changePassword(
			@PathVariable("userId") long userId,
			@ModelAttribute("changePasswordForm") @Valid ChangePasswordForm form,
			BindingResult result, RedirectAttributes redirectAttributes,
			HttpServletRequest request) throws ServletException {
		if (result.hasErrors()) {
			return "change-password";
		}
		userServiceOffer.changePassword(userId, form);

		MyUtil.flah(redirectAttributes, "success", "resetPasswordSuccess");
		request.logout();
		return "redirect:/";
	}

	@RequestMapping("/{userId}/change-email")
	public String changeEmail(Model model) {
		model.addAttribute(new ChangeEmailForm());
		return "change-email";

	}

	@RequestMapping(value = "/{userId}/change-email", method = RequestMethod.POST)
	public String changeEmail(@PathVariable("userId") long userId,
			@ModelAttribute("changeEmailForm") @Valid ChangeEmailForm form,
			BindingResult result,RedirectAttributes redirectAttributes, HttpServletRequest request) throws ServletException {
		if (result.hasErrors()) {
			return "change-email";
		}
		userServiceOffer.changeEmail(userId,form);
		MyUtil.flah(redirectAttributes, "success", "changeEmailSuccess");
		request.logout();
		return "redirect:/";

	}
	
	@RequestMapping(value = "/{userId}/delete-profile", method = RequestMethod.GET)
	public String deleteProfile(@PathVariable("userId") long userId,
			RedirectAttributes redirectAttributes, HttpServletRequest request) throws ServletException {
		
		userServiceOffer.deleteProfile(userId);
		MyUtil.flah(redirectAttributes, "success", "deleteProfileSuccess");
		request.logout();
		return "redirect:/";

	}

}
