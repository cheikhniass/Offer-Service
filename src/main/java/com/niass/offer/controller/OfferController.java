package com.niass.offer.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.niass.offer.dto.CreateOfferForm;
import com.niass.offer.dto.EditOfferForm;
import com.niass.offer.dto.EmailForm;
import com.niass.offer.entity.Offer;
import com.niass.offer.entity.OfferUser;
import com.niass.offer.service.OfferService;
import com.niass.offer.service.UserServiceOffer;
import com.niass.offer.util.MyUtil;

@Controller
@RequestMapping("/offers")
public class OfferController {

	private OfferService offerService;
	private UserServiceOffer userServiceOffer;

	@Autowired
	public OfferController(OfferService offerService,
			UserServiceOffer userServiceOffer) {
		super();
		this.offerService = offerService;
		this.userServiceOffer = userServiceOffer;
	}

	@RequestMapping(value = "/add-offer", method = RequestMethod.GET)
	public String createOffer(Model model) {
		model.addAttribute(new CreateOfferForm());
		return "create-offer";
	}

	@RequestMapping(value = "/add-offer", method = RequestMethod.POST)
	public String createOffer(
			@ModelAttribute("createOfferForm") @Valid CreateOfferForm form,
			BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "create-offer";
		}

		offerService.createOffer(form);
		MyUtil.flah(redirectAttributes, "success", "offercreatedSucess");
		return "redirect:/";
	}

	@RequestMapping(value = "/showMy-offer", method = RequestMethod.GET)
	public String showMyOffer(Model model) {
		List<Offer> offers = null;
		offers = offerService.getCurrentUserOffer();
		if (offers == null) {
			offers = new ArrayList<Offer>();
		}

		else {
			model.addAttribute("offers", offers);

		}

		return "showMy-offer";
	}

	@RequestMapping(value = "/{id}/edit-offer", method = RequestMethod.GET)
	public String editOffer(@PathVariable("id") long id, Model model) {
		Offer offer = offerService.findByOne(id);
		EditOfferForm form = new EditOfferForm();
		form.setText(offer.getText());
		model.addAttribute(form);
		return "edit-offer";

	}

	@RequestMapping(value = "/{id}/edit-offer", method = RequestMethod.POST)
	public String editOffer(@PathVariable("id") long id,
			@ModelAttribute("editOfferForm") @Valid EditOfferForm form,
			BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "edit-offer";
		}
		offerService.updateOffer(id, form);
		MyUtil.flah(redirectAttributes, "success", "offereditSucess");
		return "redirect:/";
	}

	
	@RequestMapping(value = "/show-current-offers", method = RequestMethod.GET)
	public String showcurrentOffers(Model model) {
		List<Offer> offers = null;
		offers = offerService.getCurrentOffers();
		if (offers == null) {
			offers = new ArrayList<Offer>();
		}

		else {
			model.addAttribute("offers", offers);

		}

		return "show-current-offers";
	}

	@RequestMapping(value = "/{id}/send-email", method = RequestMethod.GET)
	public String sendEmail(@PathVariable("id") long id, Model model) {

		OfferUser fromuser = MyUtil.getSessionUser();
		OfferUser toUser = userServiceOffer.findOne(id);
		EmailForm form = new EmailForm();
		form.setFrom(fromuser.getEmail());
		form.setTo(toUser.getEmail());
		model.addAttribute(form);

		return "show-email";
	}

	@RequestMapping(value = "/{id}/send-email", method = RequestMethod.POST)
	public String sendEmail(@PathVariable("id") long id,
			@ModelAttribute("emailForm") @Valid EmailForm form,
			BindingResult result, RedirectAttributes redirectAttributes) {
		
		if (result.hasErrors()) {
			return "show-email";
		}

		offerService.sendEmail(form);
		MyUtil.flah(redirectAttributes, "success", "emailSent");
		return "redirect:/";

	}
	
	@RequestMapping(value = "/{id}/remove-offer", method = RequestMethod.GET)
	public String removeOffer(@PathVariable("id") long id, RedirectAttributes redirectAttributes) {
		offerService.removeOffer(id);
		MyUtil.flah(redirectAttributes, "success", "offerRemoved");
		return "redirect:/";
	}
}
