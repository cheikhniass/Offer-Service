package com.niass.offer.mail;

import javax.mail.MessagingException;

public interface MailSender {

	public abstract void send(String to, String subject, String body)
			throws MessagingException;
	public abstract void send(String from,String to, String subject, String body)
			throws MessagingException;

}