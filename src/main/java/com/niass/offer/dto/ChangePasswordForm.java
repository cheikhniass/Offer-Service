package com.niass.offer.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.niass.offer.entity.OfferUser;

public class ChangePasswordForm {
	
	@NotNull
	@Size(min = 4, max = OfferUser.PASSWORDMAX, message="{passwordSizeError}")
	private String newPassword;
	@NotNull
	@Size(min = 4, max = OfferUser.PASSWORDMAX, message="{passwordSizeError}")
	private String  confirmPassword;
	
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
