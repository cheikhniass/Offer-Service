package com.niass.offer.dto;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.niass.offer.entity.OfferUser;
import com.niass.offer.entity.OfferUser.Role;

public class UserDetailsImpl implements UserDetails{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6139547297132941639L;
	private OfferUser user;

	public UserDetailsImpl(OfferUser user) {
		super();
		this.setUser(user);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>(user.getRoles().size()+1); 
		for(Role role : user.getRoles())
			authorities.add(new SimpleGrantedAuthority("ROLE_" + role.name()));
		
		 authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		 return authorities;
	}

	@Override
	public String getPassword() {
		return user.getPasword();
	}

	@Override
	public String getUsername() {
		return user.getEmail();
	}
		

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		//return user.isEnable();
		return true;
	}

	public OfferUser getUser() {
		return user;
	}

	public void setUser(OfferUser user) {
		this.user = user;
	}

}
