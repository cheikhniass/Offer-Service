package com.niass.offer.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.niass.offer.entity.OfferUser;

public class EmailForm {
	
	private static final int BODYMAX = 10000;
	
	
	@NotBlank
	@NotNull
	@Size(min = 1, max = BODYMAX, message = "{bodySizeError}")
	private String body;
	
	
	private String subject;
	
	@NotBlank
	@NotNull
	@Size(min = 1, max = OfferUser.EMAILMAX, message = "{emailSizeError}")
	@Pattern(regexp = OfferUser.EMAILPATTERN, message = "{emaiPatternError}")
	private String from;
	
	@NotBlank
	@NotNull
	@Size(min = 1, max = OfferUser.EMAILMAX, message = "{emailSizeError}")
	@Pattern(regexp = OfferUser.EMAILPATTERN, message = "{emaiPatternError}")
	private String to;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
