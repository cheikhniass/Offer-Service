package com.niass.offer.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.niass.offer.entity.OfferUser;


public class SignupForm {
	@NotNull
	@Size(min = 1, max = OfferUser.NAMEMAX, message="{nameSizeError}")
	private String name;
	@NotNull
	@Size(min = 4, max = OfferUser.PASSWORDMAX, message="{passwordSizeError}")
	private String password;
	@NotNull
	@Size(min = 1, max = OfferUser.EMAILMAX, message="{emailSizeError}")
	@Pattern(regexp = OfferUser.EMAILPATTERN,message ="{emaiPatternError}")
	private String emailAddress;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String toString() {
		return "SignupForm [name=" + name + ", password=" + password
				+ ", emailAddress=" + emailAddress + "]";
	}

}
