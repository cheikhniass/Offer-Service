package com.niass.offer.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.niass.offer.entity.Offer;

public class CreateOfferForm {
	@NotNull
	@NotBlank
	@Size(min = 4 ,max = Offer.TEXTSIZE, message ="{textSize}")
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	

}
