package com.niass.offer.service;

import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.niass.offer.dto.CreateOfferForm;
import com.niass.offer.dto.EditOfferForm;
import com.niass.offer.dto.EmailForm;
import com.niass.offer.entity.Offer;
import com.niass.offer.entity.OfferUser;
import com.niass.offer.mail.MailSender;
import com.niass.offer.repo.OfferRepo;
import com.niass.offer.util.MyUtil;

@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
@Service
public class OfferServiceImpl implements OfferService {
	

	private OfferRepo offerepo;
	private MailSender mailSender;

	@Autowired
	public OfferServiceImpl(OfferRepo offerepo, MailSender mailSender) {
		this.mailSender = mailSender;
		this.offerepo = offerepo;

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createOffer(CreateOfferForm form) {
		OfferUser user = MyUtil.getSessionUser();
		Offer newOffer = new Offer();
		newOffer.setText(form.getText());
		newOffer.setUser(user);
		offerepo.saveOrUpdate(newOffer);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Offer> getCurrentUserOffer() {
		long id = MyUtil.getSessionUser().getId();
		return offerepo.getOfferByLoggedInUser(id);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateOffer(long id, EditOfferForm form) {
		// TODO Auto-generated method stub
		OfferUser loggInUser = MyUtil.getSessionUser();
		Offer offer = offerepo.findByOneOffer(id);
		MyUtil.validate(!loggInUser.equals(null) && loggInUser.isAdmin()
				|| loggInUser.getId() == offer.getUser().getId(),
				"accessdenied");
		offer.setText(form.getText());
		offerepo.saveOrUpdate(offer);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Offer findByOne(long id) {
		System.out.println("offer id is " + id);
		return offerepo.findByOneOffer(id);

	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Offer> getCurrentOffers() {
		return offerepo.getCurrentOffers();

	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public void sendEmail(final EmailForm form) {
		TransactionSynchronizationManager
				.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCommit() {
						try {
							mailSender.send(form.getFrom(), form.getTo(),
									form.getSubject(), form.getBody());
						} catch (MessagingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				});

	}

	@Override
	public void removeOffer(long id) {
		Offer offer = offerepo.findByOneOffer(id);
		offerepo.remove(offer);

	}

	
}
