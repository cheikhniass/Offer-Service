package com.niass.offer.service;

import javax.mail.MessagingException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.validation.BindingResult;

import com.niass.offer.dto.ChangeEmailForm;
import com.niass.offer.dto.ChangePasswordForm;
import com.niass.offer.dto.ForgotPasswordForm;
import com.niass.offer.dto.ResetPasswordForm;
import com.niass.offer.dto.SignupForm;
import com.niass.offer.dto.UserDetailsImpl;
import com.niass.offer.entity.OfferUser;
import com.niass.offer.entity.OfferUser.Role;
import com.niass.offer.mail.MailSender;
import com.niass.offer.repo.UserOfferRepo;
import com.niass.offer.util.MyUtil;
import com.niass.offer.validator.UserEdit;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceOfferImpl implements UserServiceOffer,
		UserDetailsService {
	
	private static final Log LOG = LogFactory.getLog(UserServiceOfferImpl.class);

	private UserOfferRepo userOfferRepo;
	private PasswordEncoder encoder;
	private MailSender mailSender;

	public UserServiceOfferImpl() {

	}

	@Autowired
	public UserServiceOfferImpl(UserOfferRepo userOfferRepo,
			PasswordEncoder encoder, MailSender mailSender) {
		super();
		this.userOfferRepo = userOfferRepo;
		this.mailSender = mailSender;
		this.encoder = encoder;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		OfferUser user = userOfferRepo.findyEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return new UserDetailsImpl(user);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void signUp(SignupForm signupForm) {
		final OfferUser newUser = new OfferUser();
		newUser.setName(signupForm.getName());
		newUser.setEmail(signupForm.getEmailAddress());
		newUser.setPasword(encoder.encode(signupForm.getPassword()));
		newUser.getRoles().add(Role.UNVERIFIED);
		newUser.setVerificationCode(RandomStringUtils
				.randomAlphanumeric(OfferUser.VERIFICATIONCODE));
		userOfferRepo.saveOrUpdate(newUser);
		
		LOG.info("Thread name for sign up is" + Thread.currentThread().getName());

		TransactionSynchronizationManager
				.registerSynchronization(new TransactionSynchronizationAdapter() {

					@Override
					public void afterCommit() {
						String message = MyUtil.getHosAndPort() + "/users/"
								+ newUser.getVerificationCode() + "/verify";
						
						try {
							mailSender.send(newUser.getEmail(),
									MyUtil.getMessage("singnupsubject"),
									MyUtil.getMessage("signupbody", message));
							
						} catch (MessagingException e) {

							e.printStackTrace();
						}
					}
				});
		LOG.info("Sending email is " + Thread.currentThread().getName());

	}

	@Override
	// @PreAuthorize("hasRole('ROLE_UNVERIFIED')")
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verify(String verificationCode) {
		OfferUser user = userOfferRepo.findByVerificationCode(verificationCode);
		MyUtil.validate(user.getRoles().contains(Role.UNVERIFIED),
				"accessdenied");
		MyUtil.validate(user.getVerificationCode().equals(verificationCode),
				"invalid", "verification Code");
		user.setVerificationCode(null);
		user.setEnable(true);
		user.getRoles().remove(Role.UNVERIFIED);
		userOfferRepo.saveOrUpdate(user);

	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public void resendVerifiacationCode() {
		long loggedInuser = MyUtil.getSessionUser().getId();
		final OfferUser user = userOfferRepo.findByOne(loggedInuser);
		TransactionSynchronizationManager
				.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCommit() {
						String message = MyUtil.getHosAndPort() + "/users/"
								+ user.getVerificationCode() + "/verify";
						try {
							mailSender.send(user.getEmail(),
									MyUtil.getMessage("singnupsubject"),
									MyUtil.getMessage("signupbody", message));
						} catch (MessagingException e) {

							e.printStackTrace();
						}
					}

				});

	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public OfferUser findOne(Long userId) {

		OfferUser user = userOfferRepo.findByOne(userId);
		if (MyUtil.getSessionUser() == null) {
			user.setEmail("confidential");

		}

		return user;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void edit(Long userId, UserEdit edit) {
		OfferUser loggedInUser = MyUtil.getSessionUser();
		OfferUser userbeingEdited = userOfferRepo.findByOne(userId);
		MyUtil.validate(!loggedInUser.equals(null) && loggedInUser.getId() == userId || loggedInUser.isAdmin(),
				"accessdenied");
		userbeingEdited.setName(edit.getName());
		
		if(loggedInUser.isAdmin())
		userbeingEdited.setRoles(edit.getRoles());
		
		userOfferRepo.saveOrUpdate(userbeingEdited);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void sendForgotPasswordtoken(ForgotPasswordForm form) {
		final OfferUser user = userOfferRepo.findyEmail(form.getEmail());
		user.setForgotPasswordVerificationCode(RandomStringUtils.randomAlphanumeric(OfferUser.VERIFICATIONCODE));
		userOfferRepo.saveOrUpdate(user);
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCommit() {
				String message = MyUtil.getHosAndPort() 
						 + "/reset-password/" + user.getForgotPasswordVerificationCode();
				try {
					mailSender.send(user.getEmail(),
							MyUtil.getMessage("resetPasswordsubject"),
							MyUtil.getMessage("resetPasswordbody", message));
				} catch (MessagingException e) {

					e.printStackTrace();
				}
			}
			
		});
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void resetPassword(ResetPasswordForm form, String token,
			BindingResult result) {
		OfferUser user = userOfferRepo.findByforgotPasswordVerificationCode(token);
		MyUtil.validate(user.getForgotPasswordVerificationCode().equals(token), "invalid","Forgot Password code");
		user.setForgotPasswordVerificationCode(null);
		user.setPasword(encoder.encode(form.getNewPassword()));
		userOfferRepo.saveOrUpdate(user);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void changePassword(long userId, ChangePasswordForm form) {
	 OfferUser user = userOfferRepo.findByOne(userId);
	 OfferUser  loggedInUser = MyUtil.getSessionUser();
	 MyUtil.validate(!loggedInUser.equals(null) && loggedInUser.isAdmin() || loggedInUser.getId()== user.getId(),"accessdenied");
	 user.setPasword(encoder.encode(form.getNewPassword()));
	 userOfferRepo.saveOrUpdate(user);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void changeEmail(long userId, ChangeEmailForm form) {
		OfferUser loggedInUser = MyUtil.getSessionUser();
		OfferUser user = userOfferRepo.findByOne(userId);
		MyUtil.validate(!loggedInUser.equals(null) && loggedInUser.isAdmin() || user.getId() == loggedInUser.getId(), "accessdenied");
		user.setEmail(form.getEmail());
		userOfferRepo.saveOrUpdate(user);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteProfile(long userId) {
		OfferUser  loggedInUser = MyUtil.getSessionUser();
		final OfferUser user = userOfferRepo.findByOne(userId);
		MyUtil.validate(!loggedInUser.equals(null) && loggedInUser.isAdmin() || user.getId() == loggedInUser.getId(), "accessdenied");
		user.setEmail(null);// set the email to null so the user can sign up next time around 
		final String email = user.getEmail();
		userOfferRepo.delete(user);
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCommit() {
				String message = MyUtil.getHosAndPort() 
						 + "/signup";
				try {
					mailSender.send(email,
							MyUtil.getMessage("deleteProfilesubject"),
							MyUtil.getMessage("deleteProfilbody", message));
				} catch (MessagingException e) {

					e.printStackTrace();
				}
			}
			
		});
		
	}

}
