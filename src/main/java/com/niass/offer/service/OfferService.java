package com.niass.offer.service;

import java.util.List;

import com.niass.offer.dto.CreateOfferForm;
import com.niass.offer.dto.EditOfferForm;
import com.niass.offer.dto.EmailForm;
import com.niass.offer.entity.Offer;
import com.niass.offer.entity.OfferUser;

public interface OfferService {

	List<Offer> getCurrentUserOffer();

	void createOffer(CreateOfferForm form);

	void updateOffer(long id, EditOfferForm form);

	Offer findByOne(long id);

	List<Offer> getCurrentOffers();

	void sendEmail(EmailForm form);

	void removeOffer(long id);

 
	

}