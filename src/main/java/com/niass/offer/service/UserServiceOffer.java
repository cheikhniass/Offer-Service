package com.niass.offer.service;

import org.springframework.validation.BindingResult;

import com.niass.offer.dto.ChangeEmailForm;
import com.niass.offer.dto.ChangePasswordForm;
import com.niass.offer.dto.ForgotPasswordForm;
import com.niass.offer.dto.ResetPasswordForm;
import com.niass.offer.dto.SignupForm;
import com.niass.offer.entity.OfferUser;
import com.niass.offer.validator.UserEdit;
public interface UserServiceOffer {
     
	public abstract void signUp(SignupForm signupForm);

	public abstract void verify(String verificationCode);

	public abstract void resendVerifiacationCode();

	public abstract OfferUser findOne(Long userId);

	public abstract void edit(Long userId, UserEdit edit);

	public abstract void sendForgotPasswordtoken(ForgotPasswordForm form);

	public abstract void resetPassword(ResetPasswordForm form, String token,
			BindingResult result);

	public abstract void changePassword(long userId, ChangePasswordForm form);

	public abstract void changeEmail(long userId, ChangeEmailForm form);

	public abstract void deleteProfile(long userId);

}