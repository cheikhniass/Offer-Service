package com.niass.offer.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.niass.offer.dto.ResetPasswordForm;

@Component
public class ResetPasswordValidator extends LocalValidatorFactoryBean{
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(ResetPasswordForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors,
			final Object... validationHint) {
		super.validate(obj, errors, validationHint);
		ResetPasswordForm form = (ResetPasswordForm) obj;

		if (!errors.hasErrors()) {
			if (!form.getNewPassword().equals(form.getConfirmPassword())) {
				errors.reject("nomatch");
			}
			
		}
	}

}
