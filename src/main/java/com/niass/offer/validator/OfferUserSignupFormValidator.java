package com.niass.offer.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.niass.offer.dto.SignupForm;
import com.niass.offer.entity.OfferUser;
import com.niass.offer.repo.UserOfferRepo;
import com.niass.offer.util.MyUtil;
@Component
public class OfferUserSignupFormValidator extends LocalValidatorFactoryBean {
	@Autowired
	private UserOfferRepo userOfferRepo;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(SignupForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors,
			final Object... validationHint) {
		super.validate(obj, errors, validationHint);

		if (!errors.hasErrors()) {
			SignupForm signupForm = (SignupForm) obj;
			OfferUser user = userOfferRepo.findyEmail(signupForm.getEmailAddress());
			if (user != null) {
				errors.reject("emailAddress", MyUtil.getMessage("emailNotUnique"));
			}

		}

	}

}
