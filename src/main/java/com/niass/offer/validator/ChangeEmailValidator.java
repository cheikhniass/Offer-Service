package com.niass.offer.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.niass.offer.dto.ChangeEmailForm;


@Component
public class ChangeEmailValidator extends LocalValidatorFactoryBean {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(ChangeEmailForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors,
			final Object... validationHint) {
		super.validate(obj, errors, validationHint);
		ChangeEmailForm form = (ChangeEmailForm) obj;

		if (!errors.hasErrors()) {
			if (!form.getEmail().equals(form.getRetypeEmail())) {
				errors.reject("emailnomatch");
			}

		}
	}

}
