package com.niass.offer.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.niass.offer.dto.ForgotPasswordForm;
import com.niass.offer.entity.OfferUser;
import com.niass.offer.repo.UserOfferRepo;
import com.niass.offer.util.MyUtil;

@Component
public class ForgotPasswordValidator extends LocalValidatorFactoryBean{
	@Autowired
	private UserOfferRepo userOfferRepo;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(ForgotPasswordForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors,
			final Object... validationHint) {
		super.validate(obj, errors, validationHint);
		ForgotPasswordForm form = (ForgotPasswordForm) obj;

		if (!errors.hasErrors()) {
			OfferUser user = userOfferRepo.findyEmail(form.getEmail());
			if (user == null) {
				errors.reject("email", MyUtil.getMessage("notfound"));
				
			}
		}
	}

}
