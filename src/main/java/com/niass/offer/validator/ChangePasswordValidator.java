package com.niass.offer.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import com.niass.offer.dto.ChangePasswordForm;

@Component
public class ChangePasswordValidator extends LocalValidatorFactoryBean {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(ChangePasswordForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors,
			final Object... validationHint) {
		super.validate(obj, errors, validationHint);
		ChangePasswordForm form = (ChangePasswordForm) obj;

		if (!errors.hasErrors()) {
			if (!form.getNewPassword().equals(form.getConfirmPassword())) {
				errors.reject("nomatch");
			}

		}
	}

}
