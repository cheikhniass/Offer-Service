package com.niass.offer.validator;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.niass.offer.entity.OfferUser;
import com.niass.offer.entity.OfferUser.Role;

public class UserEdit {
	@NotNull
	@Size(min = 1, max = OfferUser.NAMEMAX, message="{nameSizeError}")
	private String name;
	
	private Set<Role> roles;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "UserEdit [name=" + name + ", roles=" + roles + "]";
	}
	
	

}
